#!/usr/bin/python3
# -*- coding: utf-8 -*-
# (c) 2018, Wouter de Geus <wdegeus(at)mirabeau.nl>

DOCUMENTATION = '''
lookup: aws_presigned_s3
author:
  - Wouter de Geus <wdegeus(at)mirabeau.nl>
version_added: 2.8
requirements:
  - boto3
  - botocore
short_description: Generate presigned S3 PUT request
description:
    See short_description

'''


EXAMPLES = '''
# lookup sample:
- name: lookup m5.large price in the us-east-1 region
  debug: msg="{{ lookup('aws_presigned_s3', 'm5.large', region=us-east-1 ) }}"

- name: lookup several instance types in in the eu-west-2 region
  debug: msg="{{ lookup('aws_ssm', 'm5.large', 'm4.large', 't3.nano', region='eu-west-2' ) }}"

'''

__metaclass__ = type

from ansible.module_utils._text import to_native
from ansible.module_utils.ec2 import HAS_BOTO3, boto3_tag_list_to_ansible_dict
from ansible.errors import AnsibleError
from ansible.plugins.lookup import LookupBase
from pprint import pformat
import json

try:
    from botocore.exceptions import ClientError
    import botocore
    import boto3
except ImportError:
    pass  # will be captured by imported HAS_BOTO3

try:
    from __main__ import display
except ImportError:
    from ansible.utils.display import Display
    display = Display()

def _boto3_conn(credentials):
    if 'boto_profile' in credentials:
        boto_profile = credentials.pop('boto_profile')
    else:
        boto_profile = None

    try:
        connection = boto3.session.Session(profile_name=boto_profile).client('pricing', 'us-east-1', **credentials)
    except (botocore.exceptions.ProfileNotFound, botocore.exceptions.PartialCredentialsError):
        if boto_profile:
            try:
                connection = boto3.session.Session(profile_name=boto_profile).client('cloudformation', 'us-east-1')
            except (botocore.exceptions.ProfileNotFound, botocore.exceptions.PartialCredentialsError):
                raise AnsibleError("Insufficient credentials found.")
        else:
            raise AnsibleError("Insufficient credentials found.")
    return connection

'''
Function to fetch current ondemand price for instancetype/region

Ondemand prices are gathered through the AWS Pricing API, which is as of now only available in us-east-1 and ap-something, so we're picking the US endpoint.
# See https://aws.amazon.com/blogs/aws/aws-price-list-api-update-new-query-and-metadata-functions/ for details.

'''

def getPresignedURL(bucketname, key):
  s3 = boto3.client('s3')

  # We use the PUT method for now, since I could not get the generate_presigned_post and curl combination to work
  # My best shot with a command such as:
  # curl -vvv -F "AWSAccessKeyId=$AKEY" -F "acl=$ACL" -F "key=$FKEY" -F "policy=$POLICY" -F "signature=$SIGN" -F "file=@$FILE" $URL
  # Returned 204 and never uploaded any files :(

  url = s3.generate_presigned_url(
    ClientMethod = 'put_object',
    Params = {'Bucket' : bucketname, 'Key' : key},
    ExpiresIn = 3600,
    HttpMethod = 'PUT'
  )

  return url
  # For future attempts:
  # Make sure everything posted is publicly readable
  fields = {"acl": "bucket-owner-full-control"}
  
  # Ensure that the ACL isn't changed and restrict the user to a length
  # between 10 and 100.
  conditions = [
      {"acl": "bucket-owner-full-control"},
      ["content-length-range", 0, 100000000]
  ]
  
  # This should not be used:
  # return s3.generate_presigned_url("put_object", Params=params, ExpiresIn=3600, HttpMethod='PUT')

  # Generate the POST attributes
  post = s3.generate_presigned_post(
      Bucket=bucketname,
      Key=key,
      Fields=fields,
      Conditions=conditions
  )
  # Generate URL from post
  display.v("URI: " + post['url'])
  # display.v("FORM: " + json.dumps(post['fields']))
  post['url_params'] = json.dumps(post['fields'])
  return post

class LookupModule(LookupBase):
    def run(self, terms, key=None, variables=None, boto_profile=None, aws_profile=None,
            aws_secret_key=None, aws_access_key=None, aws_security_token=None, region=None):
        '''
            :arg terms: bucketname and key to upload
                e.g. ['mys3bucket', '/somepath/to/upload/myfilein' ]
            :kwarg variables: ansible variables active at the time of the lookup
            :kwarg aws_secret_key: identity of the AWS key to use
            :kwarg aws_access_key: AWS seret key (matching identity)
            :kwarg aws_security_token: AWS session key if using STS
            :returns: Presigned URL to use
        '''

        if not HAS_BOTO3:
            raise AnsibleError('botocore and boto3 are required for aws ondemand price lookup.')

        credentials = {}
        if aws_profile:
            credentials['boto_profile'] = aws_profile
        else:
            credentials['boto_profile'] = boto_profile
        credentials['aws_secret_access_key'] = aws_secret_key
        credentials['aws_access_key_id'] = aws_access_key
        credentials['aws_session_token'] = aws_security_token

        client = _boto3_conn(credentials)
        results = getPresignedURL(terms[0], key)

        display.vvv("getPresignedURL lookup returned: %s" % (pformat(results)))
        # This allows quick lookups like supplying MyVar: "{{ lookup('aws_ondemand_price', 'm5.large', region='us-east-2') }}"
        return [results]

