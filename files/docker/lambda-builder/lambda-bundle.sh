#!/bin/bash
set -e
if [ ! -z "${DEBUG}" ]; then
	set -x
fi

OUTFILE=/lambda/lambda.zip
SOURCES=/lambda/

if [ -e ${OUTFILE} ]
then
	echo ERROR: Outfile $OUTFILE already exists -- refusing to overwrite.
	exit 1
fi

if [ ! -z "${S3_SOURCE}" ]
then
	echo "NOTICE: Downloading lambda sources from S3 at ${S3_SOURCE}"
	cd "${SOURCES}" && \
	wget -O /tmp/lambda_src.zip -- "${S3_SOURCE}" && \
	unzip /tmp/lambda_src.zip
fi

if [[ $(find ${SOURCES} -type f -name '*.py' | wc -l) == 0 ]]
then
	echo No source .py files found, make sure to mount /lambda where you have your source files. The resulting lambda.zip will be placed there.
	exit 1
fi

cd /lambda-build
source ./bin/activate

cp -ar ${SOURCES}/* .

# Create .pyc files to ignore
python3 -O -m compileall -b .

if [ -e ./dependencies.txt ];
then
	cd ${VIRTUAL_ENV}/lib/*/site-packages
	# Create lib of files that we do not care about
	find . > /lambda-build/lib-exclude.lst
	cd -
	pip install --no-compile -r dependencies.txt
fi

# Create .pyc files
python3 -O -m compileall -b .

# Optionally delete source .py files
if [ "${DELETESOURCE}" == "1" ]
then
	find . -name '*.py' -delete
fi

# Create ZIP file
cd ${VIRTUAL_ENV}
zip -r9q ${OUTFILE} *.py *.pyc
if [ -e ./dependencies.txt ];
then
	if [ -d ${VIRTUAL_ENV}/lib/*/site-packages ];
	then
		echo Adding lib site-packages dependencies
		cd ${VIRTUAL_ENV}/lib/*/site-packages
		# Create list of files to add
		find . -type f | sort | uniq > /lambda-build/libfiles.txt
		echo zip -gr9q  ${OUTFILE} -@
		grep -xvFf /lambda-build/lib-exclude.lst /lambda-build/libfiles.txt | zip -gr9q  ${OUTFILE} -@
	fi
	if [ -d ${VIRTUAL_ENV}/lib64/*/site-packages ];
	then
		echo Also adding lib64 site-packages dependencies
		cd ${VIRTUAL_ENV}/lib64/*/site-packages
		zip -gr9q "${OUTFILE}" .
	fi
fi
if [ ! -z "${S3_TARGET}" ]
then
	echo "NOTICE: Uploading final lambda bundle to S3 at ${S3_TARGET}"
	curl -L -vvv -D - -X PUT --upload-file "${OUTFILE}" "${S3_TARGET}"
	echo "Upload complete"
fi
echo Zip complete: `ls -la ${OUTFILE}`
